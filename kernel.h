#ifndef KERNEL_H
#define KERNEL_H

#include <stdio.h>
#include <stdlib.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "bmpfile.h"

#define RESOLUTION 8700.0
#define XCENTER -0.55
#define YCENTER 0.6
#define MAX_ITER 1000

#define COLOUR_DEPTH 255
#define COLOUR_MAX 240.0
#define GRADIENT_COLOUR_MAX 230.0

#define FILENAME "my_mandelbrot_fractal.bmp"
#define REQUIRED_ARGS 3

int parse_args(int argc, char** argv);
__global__ void mandelbrot_kernel(int width, int height, rgb_pixel_t* pixels);
__device__ void valid_pixel(int* iter, int col, int row, int xoffset, int yoffset);
__device__ void ground_colour_mix(double* color, double x, double min, double max);

#endif

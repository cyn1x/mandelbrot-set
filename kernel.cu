/*
 * The Mandelbrot Set program demonstrates a parallelisation strategy
 * for a computationally intensive problem using basic GPU features. This is
 * a simple multithread implementation that computes the Mandelbrot Set and
 * produces a corresponding Bitmap image.
 *
 * This program uses the algorithm outlined in:
 *   "Building Parallel Programs: SMPs, Clusters And Java", Alan Kaminsky
 *
 * This program requires libbmp for all bitmap operations.
 *
 * Author: Zac Vukovic (zmadden@myune.edu.au)
 *
 * - kernel [width] [height]
 * - [width]:   width of the generated image
 * - [height]:  height of the generated image
 *
 * This program uses libbmp (i.e. bmpfile.h, bmpfile.c to produce bitmap
 * formatted images from the Mandelbrot set. The program contains a number of
 * constants defined:
 *
 * RESOLUTION:  The resolution of the fractal, effectively the zoom level
 * XCENTER:     X Position of the fractal in the image
 * YCENTER:     Y Position of the fractal in the image
 * MAX_ITER:    Maximum iter value from the algorithm in the previous section
 * WIDTH:       Image size (i.e. image resolution)
 * HEIGHT:      Image size (i.e. image resolution)
 *
 * Example usage:
 * $ make
 * $ kernel 512 512
 *
 * $
 */

#include "kernel.h"

 /**
  * main() - Program entry point.
  *
  * @param argc  Number of arguments given.
  * @param argv  Arguments given to the program.
  *
  * There are 16 * 16 * 1 threads per block, although arbitrary in this case,
  * seems to be a common choice as outlined by NVIDIA. The number of blocks in
  * a grid is determined by the following arithmetic.
  *     (width / threadsPerBlock.x) * (height / threadsPerBlock.y) * (1) blocks.
  *
  * The kernel is then invocated with the number of blocks and threads per
  * block. Unified memory is used for simplicity, as the pool of memory is
  * shared between the GPU and CPU. The CPU waits for the kernel to complete
  * and then applies the data to the pixel library.
  *
  * The kernel will return the last error detected if any undefined behaviour
  * is detected. This may also return error codes from previous, asynchronous
  * launches, so a more robust error checking method would be required for
  * complex applications than this demonstration.
  *
  * Further information can be found at the following link.
  *    https://developer.nvidia.com/blog/even-easier-introduction-cuda/
  *
  * @return       0
  */
int main(int argc, char** argv)
{
    if (parse_args(argc, argv) < 0)
        exit(EXIT_FAILURE);

    bmpfile_t* bmp;
    rgb_pixel_t* pixels;
    cudaError cudaStatus;

    int width = atoi(argv[1]);
    int height = atoi(argv[2]);
    int size = sizeof(rgb_pixel_t) * (width * height);

    bmp = bmp_create(width, height, 32);
    cudaMallocManaged(&pixels, size);

    dim3 threadsPerBlock(16, 16);
    dim3 numBlocks(width / threadsPerBlock.x, height / threadsPerBlock.y);

    mandelbrot_kernel << <numBlocks, threadsPerBlock >> > (width, height, pixels);

    cudaDeviceSynchronize();

    cudaStatus = cudaGetLastError();

    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Error: Kernel detected an error\n");
        fprintf(stderr, "Message: %s\n", cudaGetErrorString(cudaStatus));
        exit(EXIT_FAILURE);
    }

    for (int col = 0; col < width; col++)
        for (int row = 0; row < height; row++)
            bmp_set_pixel(bmp, col, row, pixels[col + row * width]);

    bmp_save(bmp, FILENAME);
    bmp_destroy(bmp);

    cudaFree(pixels);

    return 0;
}

/**
 * parse_args() - Checks program arguments for validity.
 * @param argc   Number of arguments given.
 * @param argv   Arguments given to the program.
 *
 * @return       0 or -1
 */
int parse_args(int argc, char** argv)
{
    if (argc != REQUIRED_ARGS) {
        fprintf(stderr, "Error: Invalid argument. %i argument(s) "
            "entered; %i arguments required.\n",
            argc, REQUIRED_ARGS);
        fprintf(stderr, "Usage: Specify the width and height dimensions "
            "demonstration.\n");
        return -1;
    }
    if ((atoi(argv[1]) < 0) || (atoi(argv[2]) < 0)) {
        fprintf(stderr, "Error: Invalid argument. %s is not a valid "
            "dimensional property.\n", argv[1]);
        fprintf(stderr, "Usage: Dimensions cannot be negative.\n");
        return -1;
    }

    return 0;
}

/** mandelbrot_kernel() - Generates the pixels to represent the Mandelbrot Set.
  *
  * @param width    Width of the generated image
  * @param height   Height of the generated image
  * @param pixels   Array containing the RGB values for each pixel
  *
  * This kernel loops over the data array one grid-size at a time. The method
  * is coined as a grid-stride loop.
  *    blockIdx.n * blockDim.n + threadId.n
  *  = total number of threads in the grid, plus the thread computing the
       element(s)
  *
  * Further information can be found at the following link.
  *    https://developer.nvidia.com/blog/cuda-pro-tip-write-flexible-kernels-grid-stride-loops/
  */
__global__ void mandelbrot_kernel(int width, int height, rgb_pixel_t* pixels)
{
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    int row = blockIdx.y * blockDim.y + threadIdx.y;

    int xoffset = -(width - 1) / 2;
    int yoffset = (height - 1) / 2;

    for (int x = col; x < width; x += blockDim.x * gridDim.x) {
        for (int y = row; y < height; y += blockDim.y * gridDim.y) {

            int iter = 0;
            double x_col;
            double color[3];

            valid_pixel(&iter, col, row, xoffset, yoffset);

            /* Generate the colour of the pixel from the iter value */
            /* You can mess around with the colour settings to use different gradients */
            /* Colour currently maps from royal blue to red */
            x_col = (COLOUR_MAX -
                ((((float)iter / ((float)MAX_ITER) *
                    GRADIENT_COLOUR_MAX))));
            ground_colour_mix(color, x_col, 1, COLOUR_DEPTH);

            rgb_pixel_t pixel = { 0, 0, 0, 0 };
            pixel.red = color[0];
            pixel.green = color[1];
            pixel.blue = color[2];
            pixels[col + row * width] = pixel;
        }
    }
}

/**
  * valid_pixel() - Check if the x and y coordinates are apart of the set.
  *
  * @param iter     Iteration that always starts at zero
  * @param col      Column the pixel is referencing
  * @param row      Row the pixel is referencing
  * @param xoffset  Offset of the x coordinate
  * @param yoffset  Offset of the y coordinate
  *
  * Determine where in the mandelbrot set, the pixel is referencing, then
  * checks if the x and y coordinates are part of the Mandelbrot Set.
  */
__device__ void valid_pixel(int* iter, int col, int row, int xoffset, int yoffset)
{
    double dx = XCENTER + (xoffset + col) / RESOLUTION;
    double dy = YCENTER + (yoffset - row) / RESOLUTION;

    double a = 0;
    double b = 0;
    double aold = 0;
    double bold = 0;
    double zmagsqr = 0;

    while (*iter < MAX_ITER && zmagsqr <= 4.0) {
        ++* iter;
        a = (aold * aold) - (bold * bold) + dx;
        b = 2.0 * aold * bold + dy;

        zmagsqr = a * a + b * b;

        aold = a;
        bold = b;
    }
}

/**
  * ground_colour_mix() - Computes the color gradient.
  *
  * @param color    Output vector
  * @param x        Gradiant (beetween 0 and 360)
  * @param min      Variation of the RGB channels (Move3D 0 -> 1)
  * @param max      Variation of the RGB channels (Move3D 0 -> 1)
  *
  * Check Wikipedia for more details on the colour science.
  *     en.wikipedia.org/wiki/HSL_and_HSV
  *
  * @return
  */
__device__ void ground_colour_mix(double* color, double x, double min, double max)
{
    /*
     * Red = 0
     * Green = 1
     * Blue = 2
     */
    double posSlope = (max - min) / 60;
    double negSlope = (min - max) / 60;

    if (x < 60) {
        color[0] = max;
        color[1] = posSlope * x + min;
        color[2] = min;
        return;
    }
    else if (x < 120) {
        color[0] = negSlope * x + 2.0 * max + min;
        color[1] = max;
        color[2] = min;
        return;
    }
    else if (x < 180) {
        color[0] = min;
        color[1] = max;
        color[2] = posSlope * x - 2.0 * max + min;
        return;
    }
    else if (x < 240) {
        color[0] = min;
        color[1] = negSlope * x + 4.0 * max + min;
        color[2] = max;
        return;
    }
    else if (x < 300) {
        color[0] = posSlope * x - 4.0 * max + min;
        color[1] = min;
        color[2] = max;
        return;
    }
    else {
        color[0] = max;
        color[1] = min;
        color[2] = negSlope * x + 6 * max;
        return;
    }
}

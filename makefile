COMPILER = nvcc
CFLAGS = -I /usr/local/cuda-9.2/samples/common/inc -g

EXES = kernel

all: ${EXES}

kernel:   kernel.cu
	${COMPILER} ${CFLAGS} kernel.cu bmpfile.c -o kernel
clean:
	rm -f *.o *~ ${EXES} ${CFILES}
